import { Body, Controller, Get, Post } from '@nestjs/common';
import * as pg from 'pg' 
@Controller('sql-editor')

export class SqlEditorController {
    public host = ''

    @Post('connection')
    connection(@Body() req): String {
        try {
            this.host = req.host
            const { Client } = pg

            const client = new Client({
                host: req.host,
                port: 3007,
                database: req.database,
                user: req.user,
                password: req.password,
            })
            client.connect()
            client.on('error', (err) => {
                console.log('something bad has happened!', err.stack)
            })
            return 'Success connection!'
        } catch(e) {
            return 'Failed connection!'
        }
    }

    @Post('query')
    query(@Body() req): String {
        try {
            const { Client } = pg

            const client = new Client({
                host: req.host,
                port: 3007,
                database: req.database,
                user: req.user,
                password: req.password,
            })
            client.connect()
            client.on('error', (err) => {
                console.log('something bad has happened!', err.stack)
            })

            const result = client.query(req.query)
            return result
            
            client.end()
            
            return 'Success conection!'
        } catch(e) {
            return e
        }
    }
}
