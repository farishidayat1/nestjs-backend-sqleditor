import { Test, TestingModule } from '@nestjs/testing';
import { SqlEditorController } from './sql-editor.controller';

describe('SqlEditorController', () => {
  let controller: SqlEditorController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SqlEditorController],
    }).compile();

    controller = module.get<SqlEditorController>(SqlEditorController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
