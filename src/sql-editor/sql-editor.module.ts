import { Module } from '@nestjs/common';
import { SqlEditorController } from './sql-editor/sql-editor.controller';

@Module({
  controllers: [SqlEditorController]
})
export class SqlEditorModule {}
