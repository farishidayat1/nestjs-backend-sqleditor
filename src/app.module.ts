import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SqlEditorModule } from './sql-editor/sql-editor.module';

@Module({
  imports: [SqlEditorModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
